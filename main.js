var interactionsClass1  = [];
var interactionsClass2  = [];
var interactionsCommon  = [];
var interactionsOverlap = [];
var interactionsAssociation = [];
var subPathsClass1     = [];
var subPathsClass2     = [];
var subPathsCommon     = [];

var c1Thr 			= 0;
var c2Thr 			= 0;
var bothThr 		= 0;
var hideAssEdges	= false;


var myMainPanel = new Ext.form.Panel({
		region:'center', 
		contentEl: 'viewer-div', 
		autoScroll:true,
		width: 1200,
		height: 800,
		minWidth: 800,
		minHeight:600,
		title: 'Viewer', 
		collapsible: false
	});
	
initGraph = function ( _theXgmml, mpResults) {
	var jsonInput;
		$.get(_theXgmml, function(xml) {
			var $xml = $(xml)
			$.getJSON( mpResults, function( subpaths ) {
					jsonInput = subpaths;
			}).done(function() {
			
				//Empty data. In case we change pathway
				interactionsClass1= [], interactionsClass2= [], interactionsCommon= [], interactionsOverlap= [], interactionsAssociation= [], subPathsClass1= [], subPathsClass2=[], subPathsCommon=[];
				c1Thr 			= 0;
				c2Thr 			= 0;
				bothThr 		= 0;
				
				renderPathway(xml);
				initControls(jsonInput, _theXgmml);
				cy.fit();
		
				findAssocDisassoc();
				findEdges(jsonInput);
				colorEdges(hideAssEdges);

			});
		});
}

function myUpdate(c1Value, c2Value, bothValue, _hideAssEdges){
	c1Thr = c1Value/100;
	c2Thr = (-1)*c2Value/100;
	bothThr = bothValue/100;

	colorEdges(_hideAssEdges);

	var selectedEdges 			= cy.elements('edge[category = "none"]');
    var selectedEdgesClass1 	= cy.elements('edge[category = "class1"]');
	var selectedEdgesClass2 	= cy.elements('edge[category = "class2"]');
	var selectedEdgesBoth 		= cy.elements('edge[category = "both"]');	
	var selectedEdgesOverlap 	= cy.elements('edge[category = "overlap"]');
	var associationEdges 		= cy.elements('edge[category = "association"]');
	
	selectedEdges.addClass('edge[category = "none"]');
	selectedEdgesClass1.removeClass('edge[category = "none"]');
	selectedEdgesClass1.addClass('edge[category = "class1"]');
	selectedEdgesClass2.removeClass('edge[category = "none"]');
	selectedEdgesClass2.addClass('edge[category = "class2"]');
	selectedEdgesBoth.removeClass('edge[category = "none"]');
	selectedEdgesBoth.addClass('edge[category = "both"]');
	selectedEdgesOverlap.removeClass('edge[category = "none"]');
	selectedEdgesOverlap.addClass('edge[category = "overlap"]');
	if(!_hideAssEdges){
		associationEdges.removeClass('edge[category = "none"]');
		associationEdges.addClass('edge[category = "association"]');
	}
}

clearGeneColors = function(){
		//First step: Clear the GRN from previous visualization
	for(i=0; i< nodes.length; i++)
		nodes[i].data.category = 'neutral';
	
	var allnodes = cy.elements('node');
	allnodes.removeClass('down');
	allnodes.removeClass('up');
	allnodes.addClass('neutral');

}


function myArrayPush(theArray, theObj, theclass){
	var ex = theArray.indexOf(theObj);
	if(ex > -1){
		if(theArray[ex].data.class1Value < theObj.data.class1Value){
			theArray[ex].data.class1Value = theObj.data.class1Value;
			theArray[ex].data.category = theclass;
			theArray[ex].data.tooltip = theObj.data.tooltip;
		}
	}else
		theArray.push(theObj);
}

function findEdges(jsonInput){
	var edge;
	var interactions=[];
	var firstPart=[];
	var secondPart=[];
	var interactionStart;
	var interactionEnd;

	class1 = jsonInput['subPaths']['Class1'];
	class2 = jsonInput['subPaths']['Class2'];
	common = jsonInput['subPaths']['Common'];
	
	for (var i=0; i<class1.length; i++){
		subPathsClass1.push(class1[i]['score']);
		for(var j=0; j<class1[i]['EdgesIndex'].length; j++){			
				edge = class1[i]['EdgesIndex'][j];
				interactions=edge.split('(pp)');
				firstPart = interactions[0].split('hsa:');
				secondPart = interactions[1].split('hsa:');
				interactionStart = firstPart[firstPart.length-1];
				if (secondPart[0]!=" "){
					interactionEnd = secondPart[0];
				}else{
					interactionEnd = secondPart[1];
				}
				if (interactionEnd!="" && interactionStart!=""){
					var tmpLink = findLink('hsa:'+interactionStart.trim(), 'hsa:'+interactionEnd.trim() );
					if(tmpLink != undefined){
						tmpLink.data.class1Value = class1[i]['score'];
						tmpLink.data.category = 'class1';
						tmpLink.data.tooltip = '<b>score: </b>' +class1[i]['score'] + '<br><b>p-value: </b>' +class1[i]['pvalue'] + '<br><b>B&H FDR: </b>' +class1[i]['fdr'] +'<br>' +class1[i]['coverage'];
						myArrayPush(interactionsClass1 ,tmpLink, "class1");
					}
				}
		}
	}
			
	for (var i=0; i<class2.length; i++){
		subPathsClass2.push(class2[i]['score']);
		for(var j=0; j<class2[i]['EdgesIndex'].length; j++){
				edge = class2[i]['EdgesIndex'][j];
			    interactions=edge.split('(pp)');
				firstPart = interactions[0].split('hsa:');
				secondPart = interactions[1].split('hsa:');
				interactionStart = firstPart[firstPart.length-1];
				if (firstPart[0]!=" " && firstPart[0]!=""){	
					interactionStart = firstPart[0];
				}else{
					interactionStart = firstPart[firstPart.length-1];
				}
				if (secondPart[0]!=" "&& firstPart[0]!=""){
					interactionEnd = secondPart[0];
				}else{
					interactionEnd = secondPart[1];					
				}
				if (interactionEnd!="" && interactionStart!=""){
					var tmpLink = findLink('hsa:'+interactionStart.trim(), 'hsa:'+interactionEnd.trim() );
					if(tmpLink != undefined){
						tmpLink.data.class2Value = class2[i]['score'];
						if(tmpLink.data.category == 'class1')
							myArrayPush(interactionsOverlap ,tmpLink, "overlap");
							//tmpLink.data.category = 'overlap';
						else
							tmpLink.data.category = 'class2';
						tmpLink.data.tooltip = '<b>score: </b>' +class2[i]['score'] + '<br><b>p-value: </b>' +class2[i]['pvalue'] + '<br><b>B&H FDR: </b>' +class2[i]['fdr']+'<br>' +class2[i]['coverage'];
						myArrayPush(interactionsClass2 ,tmpLink, "class2");
					}
				}		
		}	
	}
	
	for (var i=0; i<common.length; i++){
		subPathsCommon.push(common[i]['score']);
		for(var j=0; j<common[i]['EdgesIndex'].length; j++){
				edge = common[i]['EdgesIndex'][j];
			    interactions=edge.split('(pp)');
				firstPart = interactions[0].split('hsa:');
				secondPart = interactions[1].split('hsa:');
				interactionStart = firstPart[firstPart.length-1];
				if (secondPart[0]!=" "){
					interactionEnd = secondPart[0];
				}else{
					interactionEnd = secondPart[1];
				}
				if (interactionEnd!="" && interactionStart!=""){
					var tmpLink = findLink('hsa:'+interactionStart.trim(), 'hsa:'+interactionEnd.trim() );
					if(tmpLink != undefined){
						tmpLink.data.commonValue = common[i]['score'];
						tmpLink.data.category = 'both';
						tmpLink.data.tooltip = '<b>score: </b>' +common[i]['score'] + '<br>' +common[i]['coverage'];
						myArrayPush(interactionsCommon ,tmpLink, "both");
					}
				}
		}
	}
}

findAssocDisassoc = function(){
	$.grep(links, function(e){
		if(e.data.target_arrow_shape.toLowerCase() == 'diamond' &&  e.data.line_style.toLowerCase() == 'dotted'){
			interactions=e.data.mapping.split('(pp)');
			firstPart = interactions[0].split('hsa:');
			secondPart = interactions[1].split('hsa:');
			interactionStart = firstPart[firstPart.length-1];
			if (secondPart[0]!=" "){
				interactionEnd = secondPart[0];
			}else{
				interactionEnd = secondPart[1];
			}
			if (interactionEnd!="" && interactionStart!=""){
				var tmpLink = findLink('hsa:'+interactionStart.trim(), 'hsa:'+interactionEnd.trim() );
				if(tmpLink != undefined)
					interactionsAssociation.push(tmpLink);
			}
		}
	});		
}
		
colorEdges = function(_hideAssEdges){
	var class1SubPathsCount = 0;
	var class2SubPathsCount = 0;
	var commonSubPathsCount = 0;
	var class1EdgesCount = 0;
	var class2EdgesCount = 0;
	var commonEdgesCount = 0;
	var overlapEdgesCount = 0;
	var overlappingNum =0;
	var assocEdgesCount = 0;

	//Clear all
	for (var l=0; l<links.length; l++){
		if (links[l].data.category != 'blank'){
			links[l].data.category='none';
		}
	}
	//Show Associations 
	if(!_hideAssEdges)
		for ( i = 0; i < interactionsAssociation.length; i++){
			interactionsAssociation[i].data.category = 'association';
			assocEdgesCount++;
			}
	
	for ( i = 0; i < interactionsClass1.length; i++)
		if(interactionsClass1[i].data.class1Value >= c1Thr){
			interactionsClass1[i].data.category = 'class1';
			class1EdgesCount++;
		}

	for ( i = 0; i < interactionsClass2.length; i++)
		if(interactionsClass2[i].data.class2Value <= c2Thr){ //Class 2 has negative values
			interactionsClass2[i].data.category = 'class2';
			class2EdgesCount++;
		}
		
	for ( i = 0; i < interactionsCommon.length; i++)
		if(interactionsCommon[i].data.commonValue >= bothThr){
			interactionsCommon[i].data.category = 'both';
			commonEdgesCount++;
		}
	for ( i = 0; i < interactionsOverlap.length; i++)
		if( (interactionsOverlap[i].data.class1Value >= c1Thr) && (interactionsOverlap[i].data.class2Value <= c2Thr) ){
			interactionsOverlap[i].data.category = 'overlap';
			overlapEdgesCount++;
		}

	for ( i = 0; i < subPathsClass1.length; i++)
		if(subPathsClass1[i] >= c1Thr)
			class1SubPathsCount++;
	for ( i = 0; i < subPathsClass2.length; i++)
		if(subPathsClass2[i] <= c2Thr)
			class2SubPathsCount++;
	for ( i = 0; i < subPathsCommon.length; i++)
		if(subPathsCommon[i] >= bothThr)
			commonSubPathsCount++;
			
	updateSubpathNumbers(class1SubPathsCount, class1EdgesCount, class2SubPathsCount, class2EdgesCount, commonSubPathsCount, commonEdgesCount, overlapEdgesCount, assocEdgesCount);	
}


getFullName = function(theGene){
	for (index = 0; index < keggIds.length; ++index) {
		value = keggIds[index];
		if (value.indexOf(theGene) > -1)  {
			return value;
		}
	}
	return undefined;
}

findLink = function(fromGene, toGene){
	if( (fromGene == undefined) || (toGene == undefined) )
		return undefined;
	theLink = fromGene + ' (pp) ' + toGene;
	var result = $.grep(links, function(e){ return e.data.mapping.indexOf(theLink) > -1});//e.data.mapping == theLink; });
	if (result.length == 0)
		return undefined;	
	return result[0];
}