MinePath Viewer provides all the functionality of the MinePath server with static data (your data). 

Simply unzip the file in an http server (e.g. Apache http, IIS, nginx) and you can view/publish your analysis results along with all the functionality of the MinePath Viewer.
More details about the functionality of the Viewer can be found in the help pages of MinePath (www.minepath.org). 

MinePath Viewer is open source software issued under the GNU General Public License v3 (https://www.gnu.org/licenses/quick-guide-gplv3.en.html).

MinePath Viewer uses the following libraries:
Ext.js (free GPL version)
Cytoscape.js
Cytoscape-ctmenu.js
Cytoscape-panzoom.js
Jquery.qtip
Jquery


