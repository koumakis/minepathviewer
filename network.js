var cy;
var positions = {}, node_map = {}, nodes = [], links = [], keggIds = [];

var selectedPathwayInGraph = undefined;

getPixRatio = function () {
    var ratio = 1;
    // To account for zoom, change to use deviceXDPI instead of systemXDPI
    if (window.screen.systemXDPI !== undefined && window.screen.logicalXDPI !== undefined && window.screen.systemXDPI > window.screen.logicalXDPI) {
        // Allowed only for values > 1
        ratio = window.screen.systemXDPI / window.screen.logicalXDPI;
    }
    else if (window.devicePixelRatio !== undefined) {
        ratio = window.devicePixelRatio;
    }
    return ratio;
};

renderPathway = function(data){
	//Clear prev network
	cy = undefined;
	positions = {}, node_map = {}, nodes = [], links = [], keggIds = [];
	
	var myPixRatio = getPixRatio();
	console.log('Pixel Ratio:' +myPixRatio);
    
	$(data).find('entry').each(function(){
        
        var entry = $(this);
        var type =  entry.attr('type');
        var graphics = entry.find('graphics');
            
        var text_valign = 'center', 
            shape = 'rectangle',
            bkg_color = '#99ff99',
            opacity = 0.9,
            border_width = 0,
            width = $(graphics).attr('width'),
            height = $(graphics).attr('height');
            
        if(type == 'gene'){
            border_width = 2;
        }else if(type == 'ortholog'){
            border_width = 2;
        }else if(type == 'compound'){
            shape = 'ellipse';
            bkg_color = '#aaaaee';
            text_valign = 'bottom';
            opacity = 1
        }else if(type == 'map'){
            shape = 'roundrectangle';
            bkg_color = '#ffffff';
        }else if( type == 'group'){
            bkg_color = '#ffffff';
            entry.find('component').each(function(){
                node_map[$(this).attr('id')].data.parent = entry.attr('id');
            });
        }
            
        var names = [];
        if(graphics.attr('name') !== undefined){
            names = graphics.attr('name').split(',');
        }
            
        var node = {};
        node.data = {
            'id': entry.attr('id'),
            'keggId': entry.attr('name').split(' '),
            'name': names[0],
            'names': names,
			'tooltip': '',
			'completeName': entry.attr('name'),
			'category': 'neutral',
            'type': type,
            'link': entry.attr('link'),
            'width': width,
            'height': height,
            'shape': shape,
            'bkg_color': bkg_color,
            'text_valign': text_valign,
            'border-width': border_width
        }
            
        keggIds.push(entry.attr('name'));
            
        node_map[entry.attr('id')] = node;
        nodes.push(node);
            
        positions[entry.attr('id')] = {
            x : +graphics.attr('x'),
            y : +graphics.attr('y')
        }
            
    });
        
    $(data).find('relation').each(function(){
        var rel = $(this), type =  rel.attr('type'), subtypes = [];

        var name = '',
            line_style = 'solid',
            target_arrow_shape = 'none',
            text = '';
				
        rel.find('subtype').each(function(){
            var sub = $(this);
			name = sub.attr('name');
                
            if(name == 'activation'){
                target_arrow_shape = 'triangle';
			}else if(name == 'compound'){
				target_arrow_shape = 'triangle';
			}else if(name == 'inhibition'){
                target_arrow_shape = 'tee';
            }else if(name == 'binding/association'){
				 line_style = 'dotted';
                target_arrow_shape = 'diamond';  //We use the diamond to find the ass/disassoc. edges
			}else if(name == 'maplink'){
                target_arrow_shape = 'diamond';
            }else if(name == 'indirect effect'){
                line_style = 'dotted';
                target_arrow_shape = 'triangle'
            }else if(name == 'state change'){
                line_style = 'dotted';
            }else if(name == 'missing interaction'){
                line_style = 'dotted';
                target_arrow_shape = 'triangle';
            }else if(name == 'phosphorylation'){
            	if(target_arrow_shape == 'none')
            		target_arrow_shape = 'triangle';	
                text = 'p+';
            }else if(name == 'dephosphorylation'){
            	if(target_arrow_shape == 'none')
            		target_arrow_shape = 'triangle';	
                text = 'p-';
            }else if(name == 'glycosylation'){
                line_style = 'dashed';
                target_arrow_shape = 'circle';
            }else if(name == 'ubiquitination'){
                line_style = 'dashed';
                target_arrow_shape = 'circle';
            }else if(name == 'methylation'){
                line_style = 'dashed';
                target_arrow_shape = 'circle';
            }else if(name == 'expression'){
                target_arrow_shape = 'triangle';
            }else if(name == 'repression'){
                target_arrow_shape = 'tee';
            }
        });
		    links.push({
                data:{
                    'source': rel.attr('entry1'),
                    'target': rel.attr('entry2'),					
					'mapping': node_map[rel.attr('entry1')].data.completeName +' (pp) ' +node_map[rel.attr('entry2')].data.completeName,
                    'name': name,
                    'reaction': type,
                    'line_style': line_style,
                    'target_arrow_shape': target_arrow_shape,
                    'text': text,
					'category': 'none',
					'class1Value': 0,
					'class2Value': 0,
					'commonValue': 0//,
                }
            });
    });

	cy = cytoscape({
        container: document.getElementById('viewer-div'),
        elements: {
            nodes : nodes,
            edges : links
        },
        style: cytoscape.stylesheet()
            .selector('node').css({
                'content': 'data(name)',
                'width':  'data(width)',
                'height':  'data(height)',
                'shape':'data(shape)',
				'background-color': 'white',
                'text-valign': 'data(text_valign)',
                'border-color': '#000000',
				'border-width': 0.5,
                'font-size': 9
            })
            .selector('edge').css({
                'content': 'data(text)',
                'target-arrow-shape': 'data(target_arrow_shape)',
                'line-style': 'data(line_style)',
				'width': 1,
                'line-color':'#777777',
                'target-arrow-color':'#777777',
                'text-valign' : 'top',
				'font-size': 11
            })
			.selector('edge[category = "none"]').css({
				'line-color': '#777777'
				,'target-arrow-color':'#777777'
				,'width': 1,
				'text-valign' : 'top',
				'font-size': 11
			  })
			  .selector('edge[category = "blank"]').css({
				'line-color': '#000000'
				,'target-arrow-color':'#000000'
				,'width': 0,
				'text-valign' : 'top',
				'font-size': 11
			  })
			.selector('edge[category = "class1"]').css({
				'line-color': 'green'
				,'target-arrow-color':'green'
				,'width': 3,
				'text-valign' : 'top',
				'font-size': 11
			  })
			.selector('edge[category = "class2"]').css({
				'line-color': 'red'
				,'target-arrow-color':'red'
				,'width': 3,
				'text-valign' : 'top',
				'font-size': 11
			  })
			  
			.selector('edge[category = "both"]').css({
				'line-color': 'black'
				,'target-arrow-color':'black'
				,'width': 3,
				'text-valign' : 'top',
				'font-size': 11
			  })
			.selector('edge[category = "overlap"]').css({
				'line-color': 'black'
				,'target-arrow-color':'black'
				,'width': 3,
				'text-valign' : 'top',
				'font-size': 11
			  })
			  .selector('edge[category = "association"]').css({
				'line-color': '#CCCC00'
				,'target-arrow-color':'#CCCC00'
				,'target-arrow-shape':'none'
				,'width': 3,
				'text-valign' : 'top',
				'font-size': 11
			  })	
			.selector('node[category = "up"]').css({
				'background-color': 'red'
			  })			  
			.selector('node[category = "down"]').css({
				'background-color': 'blue'
			  })
			.selector('node[category = "neutral"]').css({
				'background-color': 'lightgrey'
			  })
			  
			.selector('.black').css({
				'line-color': 'black'
				,'target-arrow-color':'black'
				,'width': 3
			})
			.selector(':selected').css({
				'line-color': 'yellow'				
			  })
			.selector('.faded').css({
				'opacity': 0.6,
				'text-opacity': 0.6
			  })
			  .selector('.hideInactive').css({
				'opacity': 0,
				'text-opacity': 0
			  })
			  .selector('.none').css({
				'line-color': '#777777'
				,'target-arrow-color':'#777777'
				,'width': 1
			  })
			  .selector('.class1').css({
				'line-color': 'green'
				,'target-arrow-color':'green'
				,'width': 3
			  })
			  .selector('.class2').css({
				'line-color': 'red'
				,'target-arrow-color':'red'
				,'width': 3
			  })
			  .selector('.both').css({
					'line-color': 'black'
				,'target-arrow-color':'black'
				,'width': 3
			  })
			  .selector('.up').css({
				'background-color': 'red'
			  })
			  .selector('.down').css({
				'background-color': 'blue'
			  })
			  .selector('.neutral').css({
				'background-color': 'lightgrey'
			  })
			  ,
        layout: {
            name: "preset",
            //fit: false,
            positions: positions
        },ready:function(){
            console.log('Kegg Viewer ready');
			 
			// giddy up...
			cy.elements().unselectify();
    
			cy.on('tap', 'node', function(e){
			  var node = e.cyTarget; 
			  var neighborhood = node.neighborhood().add(node);
			  cy.elements().addClass('faded');
			  
			  cy.elements().removeClass('select');
			  node.addClass('select');
			  
			  neighborhood.removeClass('faded');
			  neighborhood.addClass('mySelectedBox');
			});

			cy.on('mousedown', 'node', function(e){
				var node = e.cyTarget; 

				var geneName = this._private.data.name;
				var completeName = this._private.data.completeName.replace("path:", "");
				selectedPathwayInGraph = completeName;
				if(this._private.data.type =='map'){
					node.qtip({
						content: {
							title: geneName,
							text: '<a href="http://www.genome.jp/kegg-bin/show_pathway?' +completeName +'" target="_blank"> Pathway info (KEGG)</a><br>Load Pathway in MinePath?<br><button onclick="myFunctionChangePathway()">Load</button>'
						},
						style: {classes: 'qtip-bootstrap'}
					});

					return;
				}

				var geneNames = this._private.data.names;
				textForToolTip = '<a href="' +this._private.data.link +'" target="_blank"><b>Gene Info (kegg)</b></a><br>';
				var epgaToolTip = '<br><b><div style="background-color:lightgreen;max-height:300px;overflow:auto">Known Targeting Alleles (EPGA)';
				var existEpga = false;
				//epgaData has been initialized in main.js
				 for(k=0; k < this._private.data.names.length; k++){
					 var objEpga = epgaData[this._private.data.names[k].trim()];
					 if (objEpga != undefined){
						 var epgaAttr = epgaData[this._private.data.names[k].trim()];
						 var epgaName = Object.keys(epgaAttr)[0];
						 for(m=0; m < epgaAttr[epgaName].alleles.length; m++)
							epgaToolTip = epgaToolTip +'<br><a href="http://www.pharmgkb.org/rsid/' +epgaAttr[epgaName].alleles[m] +'" target="_blank">'+epgaAttr[epgaName].alleles[m] +'</a> ' +epgaName;
						 existEpga = true;
					 }
				 }
				epgaToolTip = epgaToolTip + '</div>';
				if(!existEpga)
					epgaToolTip = '';			
				
				node.qtip({
					content: {
						title: geneName,
						text: textForToolTip + epgaToolTip
					},
					style: {classes: 'qtip-bootstrap'}
				});


			});
			cy.on('mousedown', 'edge', function(e){
				if(this._private.data.category == "none")
					return;
				var edge = e.cyTarget; 				
				edge.qtip({
					content: {text: '<font size="2">' +this._private.data.tooltip.replace("coverage:", "") +'</font>'},
                    style: {classes: 'qtip-bootstrap'}
                });				
			});
			
			cy.on('cxttapend', function(e){

				if( e.cyTarget === cy ){
						context.init({preventDoubleContext: false});
						context.attach(this, [
							{header: 'Network Features'},
							{text: 'Delete Inactive Genes', action: function(e){
									removeNodes();
								}
							},
							{text: 'Delete Inactive relations', action: function(e){
									removeEdges();
								}
							},
							{text: 'Delete Selected', action: function(e){
									var collection = cy.elements('node.select');
									cy.remove( collection );
								}
							},
							{divider: true},
							{header: 'Topology'},
							{text: 'Set Random Topology', action: function(e){
									var layout = cy.makeLayout({  name: 'cose' });
									layout.run();
								}
							},
							{text: 'Reset KEGG Topology', action: function(e){
									var layout = cy.makeLayout({  name: "preset", positions: positions });
									layout.run();
								}
							}
						]);
						context.settings({compress: true});
				  }
			});
			cy.on('tap', function(e){
			  if( e.cyTarget === cy ){
				cy.elements().removeClass('faded');				
			  }
			});
			cy.on('select', function(e){
				var el = e.cyTarget; 
				var neighborhood = el.neighborhood().add(el);
				cy.elements().addClass('faded');
				neighborhood.removeClass('faded');
			});
		}
		,pixelRatio: myPixRatio
		,selectionType : 'single'
		
    });
	cy.panzoom({});
};

removeInactive = function(){
	removeNodes();
	removeEdges();
}

removeEdges = function(){
	var collection = cy.elements("edge[category = 'none']");
	cy.remove( collection );
}

removeNodes = function(){
	var  removedList = [];
	for ( var k = cy.nodes().length-1; k >-1; k--) {				
		if(cy.nodes()[k]._private.data.type != 'gene')
				continue;
		var n = cy.$('#' +cy.nodes()[k]._private.data.id) ;
		var isInactive = true;
		var nEdges = n.connectedEdges();
		for( var j=0; j<nEdges.length; j++){
			if(nEdges[j]._private.data.category != 'none'){
				isInactive = false;
				break;
			}
		}
		if(isInactive){
			cy.remove(n);
			removedList.push(cy.nodes()[k]._private.data.id);
		}
	}
	for ( var k = cy.nodes().length-1; k >-1; k--) {		
		if(k >cy.nodes().length-1)	
			k = cy.nodes().length-1;		
		if(cy.nodes()[k]._private.data.type != 'group')
			continue;
		var n = cy.$('#' +cy.nodes()[k]._private.data.id) ;
		if(n[0]._private.children.length == 0)
			cy.remove(n);

	}
}
function myFunctionChangePathway(){
	initGraph("pathwaysRepo/xml/" +selectedPathwayInGraph +".xml" , "myAnalysis/" +MAfile +"." +selectedPathwayInGraph +".xgmml.json");
}