var myControlsPanel = new Ext.form.Panel({
		region: 'west',
		contentEl: 'controls-div', 
		autoScroll:true,
		title: 'Controls', 
		collapsible: true
});

initControls = function(json, pathName){

	infoset.items.items[0].setValue(	'Pathway <span style="font-weight:bold;">' +pathName.replace("pathwaysRepo/xml/","") +'</span>') ;
	infoset.items.items[1].setValue(	'Micrarray file <br><span style="font-weight:bold;">'+json.MicroArray +'</span>');

	class1set.items.items[0].setValue( 	'Name <span style="font-weight:bold;">' +json.class1 +'</span>');
	class1Slider.setMinValue(parseInt(json.class1min*100));
	class1Slider.setMaxValue(1+parseInt(json.class1max*100));
	class1Slider.setValue(parseInt(json.class1thr*100));
	
	class2set.items.items[0].setValue( 	'Name <span style="font-weight:bold;">' +json.class2 +'</span>');
	class2Slider.setMinValue(parseInt( Math.abs(json.class2min)*100));
	
	class2Slider.setMaxValue(parseInt( 1+ Math.abs(json.class2max)*100));
	class2Slider.setValue(parseInt( Math.abs(json.class2thr)*100));

	overlappingset.items.items[0].setValue( 	'Coding <span style="font-weight:bold;">Black</span>');

	bothset.items.items[1].setValue(	'Minimum <span style="font-weight:bold;">' +parseInt(json.bothUpThr*100) +'%</span>');
	bothSlider.setMinValue(parseInt(json.bothUpThr*100));
	bothSlider.setMaxValue(100);
	bothSlider.setValue(parseInt(json.bothUpThr*100));
	
	//Re initialize in case of new/changed pathway
	updateSubpathNumbers(0, 0, 0, 0, 0, 0, 0);
	Ext.getCmp('hideAssEdges').setValue(false);
}

updateSubpathNumbers = function(redNum, redNumEdges, blueNum, blueNumEdges, orangeNum, orangeNumEdges,overlappingNum, bindingNum){
	class1set.items.items[2].setValue(	'<span style="color:green;font-weight:bold;">'+redNum +'</span> sub-paths <br><span style="color:green;font-weight:bold;">'+redNumEdges +'</span> unique reactions.');
	class2set.items.items[2].setValue(	'<span style="color:red;font-weight:bold;">'+blueNum +'</span> sub-paths <br><span style="color:red;font-weight:bold;">'+blueNumEdges +'</span> unique reactions.');
	bothset.items.items[5].setValue(	'<span style="color:#CCCC00;font-weight:bold;">'+bindingNum +'</span> Association/Dissociation edges.');

	overlappingset.items.items[1].setValue(	'<span style="color:black;font-weight:bold;">'+overlappingNum +' </span>overlapping edges</span> ');
	bothset.items.items[2].setValue(	'<span style="color:black;font-weight:bold;">'+orangeNum +'</span> sub-paths <br><span style="color:black;font-weight:bold;">'+orangeNumEdges +'</span> unique reactions.');
	}
	
var class1Slider = new Ext.slider.Single ({
    name: 'c1Slider',
	width: 150,
    decimalPrecision: 2,
    increment: 1 ,
    useTips: true,
    listeners: {
    	changecomplete: function(f, newVal, oldVal) {
                //console.log( newVal );
                controlsValueChanged();
            }
     }
}); 

var class2Slider = new Ext.slider.Single ({
    name: 'c2Slider',
    width: 150,
	decimalPrecision: 2,
	increment: 1 ,
	useTips: true,
	listeners: {
    	changecomplete: function(f, newVal, oldVal) {
                //console.log( newVal );
                controlsValueChanged();
            }
     }
}); 

var bothSlider = new Ext.slider.Single ({
    name: 'bothSlider',
    width: 150,
	maxValue: 1,
	decimalPrecision: 2,
	increment: 1 ,
	tipText: function(thumb){
    	return String(thumb.value) + '%';
	}, 
	useTips: true,
	listeners: {
    	changecomplete: function(f, newVal, oldVal) {
                controlsValueChanged();
            }
     }
}); 
	
var controlsValueChanged = function(){
	var c1Value = class1Slider.getValues()[0];
	var c2Value = class2Slider.getValues()[0];
	var bothValue = bothSlider.getValues()[0];
	myUpdate(c1Value, c2Value, bothValue, Ext.getCmp('hideAssEdges').checked); // Updates the cytoscape graph (Function can be found at main.js)	
}

var infoset = new Ext.form.FieldSet({
        collapsible: false,
        border: false,
        defaults: {anchor: '100%'},
        layout: 'anchor',
        items :[{
        	xtype: 'displayfield',
        	value: 'Pathway <span style="font-weight:bold;"> +pathwayName +</span>'        	
       		},{
            xtype: 'displayfield',
            value: 'Micrarray file <span style="font-weight:bold;">+maFile +</span>'
       		}
        ]
	}
);

var class1set = new Ext.form.FieldSet({
        collapsible: false,
        flex: 1,
        title: 'Class 1',
        defaults: {anchor: '100%'},
        layout: 'anchor',
        items :[{
        	xtype: 'displayfield',
        	value: 'Name <span style="font-weight:bold;"> </span>'        	
       		},{
            xtype: 'displayfield',
            value: 'Coding <span style="color:green;font-weight:bold;">Green</span>'//'Coding <span style="color:red;font-weight:bold;">Red</span>'
			}, {
            xtype: 'displayfield',
        	value: 'Viewing <span style="font-weight:bold;">  </span> sub-paths'
     		}, {     			
            xtype: 'displayfield',
        	value: '<span style="font-weight:bold;">Set Threshold</span>'
     		},
     		class1Slider
        ]
	}
);

var class2set = new Ext.form.FieldSet({
        collapsible: false,
        flex: 1,
        title: 'Class 2',
        defaults: {anchor: '100%'},
        layout: 'anchor',
        items :[{
        	xtype: 'displayfield',
        	value: 'Name <span style="font-weight:bold;"> </span>'
       		},{
            xtype: 'displayfield',
            value: 'Coding <span style="color:red;font-weight:bold;">Red</span>'//'Coding <span style="color:blue;font-weight:bold;">Blue</span>'
			}, {
            xtype: 'displayfield',
        	value: 'Viewing <span style="font-weight:bold;">  </span> sub-paths'
     		}, {     
            xtype: 'displayfield',
        	value: '<span style="font-weight:bold;">Set Threshold</span>'
     		},
     		class2Slider
        ]
	}
);

var overlappingset = new Ext.form.FieldSet({
        collapsible: false,
        flex: 1,
        title: 'Overlapping (Green & Red)',
        defaults: {anchor: '100%'},
        layout: 'anchor',
        items :[{
            xtype: 'displayfield',
            value: 'Coding <span style="color:Black;font-weight:bold;">Black</span>'//'Coding <span style="color:blue;font-weight:bold;">Blue</span>'
       		}, {
            xtype: 'displayfield',
        	value: 'Viewing <span style="font-weight:bold;">  </span> sub-paths'
     		}
        ]
	}
);

var bothset = new Ext.form.FieldSet({
        collapsible: false,
        flex: 1,
        title: 'Both Classes',
        defaults: {anchor: '100%'},
        layout: 'anchor',
        items :[{
            xtype: 'displayfield',
            value: 'Coding <span style="color:black;font-weight:bold;">Black</span>'// 'Coding <span style="color:orange;font-weight:bold;">Orange</span>'
       		}, {
            xtype: 'displayfield',
        	value: 'Threshold <span style="font-weight:bold;"> +boththr +</span>'
     		}, {
            xtype: 'displayfield',
        	value: 'Viewing <span style="font-weight:bold;">  </span> sub-paths'
     		}, { 
            xtype: 'displayfield',
        	value: '<span style="font-weight:bold;">Set Threshold</span>'
     		},
     		bothSlider
     		,{
     		xtype: 'displayfield',
        	value: '<span style="color:#CCCC00;font-weight:bold;">0</span> Association/Dissociation edges.'
     		},
            {
            	xtype: 'checkbox',
                boxLabel: 'Hide Edges',
                id: 'hideAssEdges',
                name: 'hideAssEdges',
                checked: false,
				listeners: {
                    change: function (checkbox) {
						controlsValueChanged();  
                    }
                }
            }
     		
        ]
});
changePathwayButton = new Ext.button.Button({text: 'Change Pathway ',region: 'center',margin: '10, 10, 10, 10', handler: function() {
			pathToViewWindow.show();
		}
	});

myControlsPanel.add(infoset);

myControlsPanel.add(class1set);
myControlsPanel.add(class2set);
myControlsPanel.add(overlappingset);
myControlsPanel.add(bothset);
myControlsPanel.add( new Ext.menu.Separator() );
myControlsPanel.add(changePathwayButton);