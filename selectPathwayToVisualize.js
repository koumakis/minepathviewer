var pathToViewWindow;
var firstTimePathView = true;
var MAfile ;
var epgaData;

function addPathwayView() {
	var xmlFileName = "pathwayStats.xml";
	
	$.getJSON( "epga.json", function( _epgaData ) {
		epgaData = _epgaData;
	});
				
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		    var xmlDoc = this.responseXML;
			MAfile = xmlDoc.getElementsByTagName("Experiment_information")[0].childNodes[1].innerHTML;	
			mp.innerText = "MinePath Analysis\n" +MAfile;
		}
	};
	xhttp.open("GET", xmlFileName, true);
	xhttp.send();


	
	curPathwayViewing = 'Pathway View';
	pathToViewWindow = new Ext.Window({title: 'Select pathway to visualize', maxHeight:600 ,width:1050});

	Ext.define('Pathway',{
        extend: 'Ext.data.Model',
        fields: [
            // set up the fields mapping into the xml doc
            {name: 'KeggID', mapping: 'name'},
            {name: 'title', mapping: 'title'},
            {name: 'numOfGenes', mapping: 'numOfGenes', sortType: 'asInt'},
            {name: 'numOfSubPaths', mapping: 'numOfSubPaths', sortType: 'asInt'},
            {name: 'pwA', mapping: 'pwA', sortType: 'asInt'},
            {name: 'numOfInteractions', mapping: 'numOfInteractions', sortType: 'asInt'},
            {name: 'activeInteractions', mapping: 'activeInteractions', sortType: 'asInt'},
            {name: 'score', mapping: 'score', sortType: 'asFloat'},            
            {name: 'pwDS', mapping: 'pwDS', sortType: 'asFloat'},
            {name: 'p-value', mapping: 'p-value', sortType: 'asFloat'},
            {name: 'numOfSubPathsOverThrClass1', mapping: 'numOfSubPathsOverThrClass1', sortType: 'asInt'},
            {name: 'numOfSubPathsOverThrClass2', mapping: 'numOfSubPathsOverThrClass2', sortType: 'asInt'},
            {name: 'numOfSubPathsCommon', mapping: 'numOfSubPathsCommon', sortType: 'asInt'}
        ]
    });
	
	// create the Data Store
	var store = Ext.create('Ext.data.Store', {
	    model: 'Pathway',
	    autoLoad: true,
	    proxy: {
	        // load using HTTP
	        type: 'ajax',
	        url: xmlFileName,
	        reader: {
	            type: 'xml',
	            record: 'Pathway',
	            idProperty: 'name' 
	        }
	    },
	    sortOnLoad: true, 
	    sorters: { property: 'p-value', direction : 'ASC' }
	});
	
	// create the grid
	var grid = Ext.create('Ext.grid.Panel', {
		autoScroll:true,
	    store: store,
	    columns: [
	        {text: "Kegg ID", width: 100, dataIndex: 'KeggID', sortable: true},
	        {text: "Title", width: 150, dataIndex: 'title', sortable: true},
	        {text: "Genes", width: 50, dataIndex: 'numOfGenes', sortable: true},
	        {text: "SubPaths", width: 55, dataIndex: 'numOfSubPaths', sortable: true},
	        {text: "Interactions", width: 70, dataIndex: 'numOfInteractions', sortable: true}, 
	        {text: "Active SubPaths", width: 100, dataIndex: 'pwA', sortable: true},	        
	        {text: "Active Interactions", width: 110, dataIndex: 'activeInteractions', sortable: true}, 
	        {text: "P-Value", width: 140, dataIndex: 'p-value', sortable: true},
	        {text: "<font color=green>For Class 1</font>", width: 75, dataIndex: 'numOfSubPathsOverThrClass1', sortable: true},
	        {text: "<font color=red>For Class 2</font>", width: 75, dataIndex: 'numOfSubPathsOverThrClass2', sortable: true},
	        {text: "<font color=black>Common</font>", width: 75, dataIndex: 'numOfSubPathsCommon', sortable: true}
	    ]
	    ,height: 520
	});
	
	pathToViewWindow.add(grid);
	
	pathToViewWindow.add(new Ext.button.Button({ text: 'Visualize Pathway', scale:'medium', width: '100%', bodyStyle: 'align:center', handler: function(){
				if (grid.getSelectionModel().hasSelection()) {
					curPathwayViewing = grid.getSelectionModel().getSelection()[0].data.KeggID;
					pathToViewWindow.setVisible(false);
					initGraph("pathwaysRepo/xml/" +curPathwayViewing.replace(".xgmml", ".xml") , "myAnalysis/" +MAfile +"." +curPathwayViewing +".json");					
				}else
					Ext.Msg.alert('Pathway Selection', "Please select a Pathway to visualize with MinePath.");
		}
	}) );
	pathToViewWindow.show();
}
