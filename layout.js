Ext.require(['*']);

Ext.onReady(function(){
	addPathwayView();
	
    var viewport = Ext.create('Ext.Viewport', {    	
        layout: {
            type: 'border',
            border: false,
            padding: 0
        },
        items: [
			myControlsPanel,
			myMainPanel
		]
    });	
	
});